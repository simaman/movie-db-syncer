<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\CardImage;
use App\Models\Cast;
use App\Models\Cert;
use App\Models\Director;
use App\Models\Genre;
use App\Models\KeyArtImage;
use App\Models\Reviewer;
use App\Models\Sclass;
use App\Models\Show;
use App\Models\User;
use App\Models\Video;
use App\Models\VideoAlternative;
use App\Models\ViewingWindow;
use App\Services\Sync;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\DocBlock\Tags\Author;

class DownloadController extends Controller
{
    public function index()
    {

        set_time_limit(60*5);
        (new Sync())->sync();

        return true;
    }
}
