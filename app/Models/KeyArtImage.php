<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class KeyArtImage extends Model
{
    protected $guarded = [];

    protected static function booted()
    {
        parent::booted();

        self::deleted(function ($image) {
            Storage::disk('local')->delete($image->show->remote_id . '/keyArtImages/' . $image->file_name);
        });
    }

    public function show()
    {
        return $this->belongsTo(Show::class);
    }
}
