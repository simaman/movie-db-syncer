<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    public static $certs = ['U', 'PG', '12', '15', '18'];
    public static $classes = ['Movie'];

    protected $guarded = [];

    public function reviewer()
    {
        return $this->hasOne(Reviewer::class);
    }

    public function cardImages()
    {
        return $this->hasMany(CardImage::class);
    }

    public function keyArtImages()
    {
        return $this->hasMany(KeyArtImage::class);
    }

    public function cast()
    {
        return $this->belongsToMany(Cast::class, 'show_cast');
    }

    public function directors()
    {
        return $this->belongsToMany(Director::class, 'show_director');
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'show_genre');
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }
}
