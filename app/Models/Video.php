<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Video extends Model
{
    protected $guarded = [];

    protected static function booted()
    {
        parent::booted();

        self::deleted(function ($video) {
            Storage::disk('local')->deleteDir($video->show->remote_id . '/videos');
        });
    }

    public function alternatives()
    {
        return $this->hasMany(VideoAlternative::class);
    }

    public function show()
    {
        return $this->belongsTo(Show::class);
    }
}
