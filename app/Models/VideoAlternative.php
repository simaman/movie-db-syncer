<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class VideoAlternative extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected static function booted()
    {
        parent::booted();

        self::deleted(function ($alternative) {
            Storage::disk('local')->delete(
                $alternative->video->show->remote_id . '/videos/' . $alternative->video->id . '/' .
                'alternatives/' . $alternative->file_name);
        });
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }
}
