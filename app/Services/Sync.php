<?php

namespace App\Services;

use App\Models\CardImage;
use App\Models\Cast;
use App\Models\Cert;
use App\Models\Director;
use App\Models\Genre;
use App\Models\KeyArtImage;
use App\Models\Reviewer;
use App\Models\Sclass;
use App\Models\Show;
use App\Models\Video;
use App\Models\VideoAlternative;
use App\Models\ViewingWindow;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class Sync
{
    public $showDbInstance;
    public function sync()
    {
        $response = Http::accept('application/json')->get(env('SYNC_URL'));
        $movies = $response->body();
        $movies = json_decode(mb_convert_encoding($movies, 'UTF-8', 'UTF-8'));

        foreach ($movies as $movie) {
            $this->syncInstance($movie);
        }
    }

    public function syncInstance(\stdClass $movie)
    {
        $show = Show::firstOrNew(['remote_id' => $movie->id]);

        if ($show->sum == $movie->sum || $show->last_updated < $movie->lastUpdated && $show->exists) {
            return;
        }

        $show->body = $movie->body;

        $certName = $movie->cert ?? null;

        if ($certName) {
            $cert = Cert::firstOrCreate(['type' => $certName]);
            $show->cert_id = $cert->id;
        }

        $className = $movie->class ?? null;

        if ($className) {
            $class = Sclass::firstOrCreate(['name' => $className]);
            $show->class_id = $class->id;
        }

        $show->duration = $movie->duration;
        $show->headline = $movie->headline;
        $show->last_updated = $movie->lastUpdated;
        $show->quote = $movie->quote ?? null;
        $show->rating = $movie->rating ?? null;

        $reviewAuthorName = $movie->reviewAuthor ?? null;

        if ($reviewAuthorName) {
            $reviewer = Reviewer::firstOrCreate(['name' => $reviewAuthorName]);
            $show->reviewer_id = $reviewer->id;
        }

        $show->skyGoId = $movie->skyGoId ?? null;
        $show->skyGoUrl = $movie->skyGoUrl ?? null;
        $show->synopsis = $movie->synopsis;
        $show->url = $movie->url;
        $show->year = $movie->year;

        $show->save();

        $this->showDbInstance = $show;

        if (isset($movie->viewingWindow)) {
            ViewingWindow::updateOrCreate(['show_id' => $show->id
            ], ['start_date' => $movie->viewingWindow->startDate ?? null,
                'way_to_watch' => $movie->viewingWindow->wayToWatch ?? null,
                'end_date' => $movie->viewingWindow->endDate ?? null,
            ]);
        }

        $this->syncImages($movie, 'cardImages');
        $this->syncImages($movie, 'keyArtImages');
        $this->syncManyToManyType($movie, 'cast');
        $this->syncManyToManyType($movie, 'directors');
        $this->syncManyToManyType($movie, 'genres');

        $this->syncVideos($movie);
    }

    public function syncImages(\stdClass $movie, string $imageType)
    {
        $show = $this->showDbInstance;
        $movie->$imageType ??= [];
        $names = array_column($movie->$imageType, 'url');
        $currentNames = $show->$imageType->pluck('url');
        $newNames = collect($names)->diff($currentNames);
        $removedNames = $currentNames->diff($names);
        $cards = [];

        $imageTypeClass = get_class($show->$imageType()->getRelated());
        foreach ($movie->$imageType as $image) {
            if ($newNames->contains($image->url)) {
                try {
                    $contents = file_get_contents($image->url);
                } catch (\Exception $e) {
                    continue;
                }

                if ($contents && Storage::disk('local')->put(
                        $show->remote_id . "/{$imageType}/" . basename($image->url), $contents)
                ) {
                    $card = new $imageTypeClass;
                    $card->file_name = basename($image->url);
                    $card->url = $image->url;
                    $card->h = $image->h;
                    $card->w = $image->w;
                    $cards[] = $card;
                }
            }
        }

        $imageTypeClass::whereIn('url', $removedNames)->get()->map(fn($name) => $name->delete());
        $show->$imageType()->saveMany($cards);
    }

    public function syncManyToManyType(\stdClass $movie, string $type)
    {
        $show = $this->showDbInstance;
        $typeClass = get_class($show->$type()->getRelated());

        if (isset($movie->$type)) {
            $names = array_column($movie->$type, 'name');
            $currentNames = $show->$type->pluck('name');
            $newNames = collect($names)->diff($currentNames);
            $removedNames = $currentNames->diff($names);

            foreach ($newNames as $name) {
                $show->$type()->attach($typeClass::firstOrCreate(['name' => $name])->id);
            }
            $removedIds = $typeClass::whereIn('name', $removedNames)->get()->pluck('id');

            $show->$type()->detach($removedIds);
        }
    }

    public function syncVideos(\stdClass $movie)
    {
        $show = $this->showDbInstance;
        $movie->videos ??= [];
        $names = array_column($movie->videos, 'url');
        $currentNames = $show->videos->pluck('url');
        $newNames = collect($names)->diff($currentNames);
        $removedNames = $currentNames->diff($names);

        foreach ($movie->videos as $video) {
            if ($newNames->contains($video->url)) {
                try {
                    $contents = file_get_contents($video->url);
                } catch (\Exception $e) {
                    continue;
                }

                $dbVideo = new Video();
                $dbVideo->file_name = basename($video->url);
                $dbVideo->type = $video->type;
                $dbVideo->show_id = $show->id;
                $dbVideo->save();
                if ($contents) {
                    Storage::disk('local')->put(
                        $show->remote_id . '/videos/' . $dbVideo->id . '/' . basename($video->url), $contents);
                }
                $dbAlternatives = [];
                foreach ($videos->alternatives ?? [] as $alternative) {
                    try {
                        $contents = file_get_contents($alternative->url);
                    } catch (\Exception $e) {
                        continue;
                    }

                    if ($contents && Storage::disk('local')->put(
                            $show->remote_id . '/videos/' . $dbVideo->id . '/alternatives/' . basename($alternative->url), $contents)
                    ) {
                        $dbAlternative = new VideoAlternative();
                        $dbAlternative->quality = $alternative->quality;
                        $dbAlternative->url = $alternative->url;
                        $dbAlternative->file_name = basename($alternative->url);
                    }
                    $dbAlternatives[] = $dbAlternative;
                }

                $dbVideo->saveMany($dbAlternatives);
            } elseif ($currentNames->contains($video->url)) {
                //check if there are alternatives to add or delete
                $currentVideo = $show->videos()->where('url', $video->url)->get();
                $video->alternatives ??= [];
                $names = array_column($video->alternatives, 'url');
                $currentNames = $currentVideo->alternatives->pluck('url');
                $newNames = collect($names)->diff($currentNames);
                $removedNames = $currentNames->diff($names);
                $dbAlternatives = [];
                foreach ($video->alternatives as $alternative) {
                    if ($newNames->contains($alternative->url)) {
                        try {
                            $contents = file_get_contents($alternative->url);
                        } catch (\Exception $e) {
                            continue;
                        }

                        if ($contents && Storage::disk('local')->put(
                                $show->remote_id . '/videos/' . $currentVideo->id . '/alternatives/' . basename($alternative->url), $contents)
                        ) {
                            $dbAlternative = new VideoAlternative();
                            $dbAlternative->quality = $alternative->quality;
                            $dbAlternative->url = $alternative->url;
                            $dbAlternative->file_name = basename($alternative->url);
                            $dbAlternatives[] = $dbAlternative;
                        }
                    }
                }

                $currentVideo->saveMany($dbAlternatives);
                VideoAlternative::whereIn('url', $removedNames)->get()->map(fn($name) => $name->delete());
            }
        }

        Video::whereIn('url', $removedNames)->get()->map(fn($name) => $name->delete());
    }
}
