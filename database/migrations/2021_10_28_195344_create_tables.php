<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('reviewers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('certs', function (Blueprint $table) {
            $table->id();
            $table->char('type');
            $table->timestamps();
        });

        Schema::create('classes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('shows', function (Blueprint $table) {
            $table->id();
            $table->string('remote_id')->unique();
            $table->text('body')->nullable();
            $table->bigInteger('cert_id')->unsigned()->nullable();
            $table->bigInteger('class_id')->unsigned()->nullable();
            $table->unsignedMediumInteger('duration')->nullable();
            $table->string('headline')->nullable();
            $table->date('last_updated')->nullable();
            $table->string('quote')->nullable();
            $table->unsignedSmallInteger('rating')->nullable();
            $table->text('synopsis')->nullable();
            $table->string('url')->nullable();
            $table->string('sum')->nullable();
            $table->unsignedSmallInteger('year')->nullable();
            $table->bigInteger('reviewer_id')->unsigned()->nullable();
            $table->string('skyGoId')->nullable();
            $table->string('skyGoUrl', 2048)->nullable();
            $table->foreign('reviewer_id')->references('id')->on('reviewers');
            $table->foreign('cert_id')->references('id')->on('certs');
            $table->foreign('class_id')->references('id')->on('classes');
            $table->timestamps();
        });

        Schema::create('card_images', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('file_name');
            $table->unsignedSmallInteger('w');
            $table->unsignedSmallInteger('h');
            $table->bigInteger('show_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows');
            $table->timestamps();
        });

        Schema::create('cast', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('show_cast', function (Blueprint $table) {
            $table->bigInteger('show_id')->unsigned();
            $table->bigInteger('cast_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows');
            $table->foreign('cast_id')->references('id')->on('cast');
        });

        Schema::create('directors', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('show_director', function (Blueprint $table) {
            $table->bigInteger('show_id')->unsigned();
            $table->bigInteger('director_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows');
            $table->foreign('director_id')->references('id')->on('directors');
        });

        Schema::create('genres', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('show_genre', function (Blueprint $table) {
            $table->bigInteger('show_id')->unsigned();
            $table->bigInteger('genre_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows');
            $table->foreign('genre_id')->references('id')->on('genres');
        });

        Schema::create('key_art_images', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('file_name');
            $table->unsignedSmallInteger('w');
            $table->unsignedSmallInteger('h');
            $table->bigInteger('show_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows');
            $table->timestamps();
        });

        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('type');
            $table->string('file_name');
            $table->string('url');
            $table->bigInteger('show_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows');
            $table->timestamps();
        });

        Schema::create('video_alternatives', function (Blueprint $table) {
            $table->id();
            $table->string('quality');
            $table->string('file_name');
            $table->string('url');
            $table->bigInteger('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('viewing_windows', function (Blueprint $table) {
            $table->id();
            $table->date('start_date');
            $table->string('way_to_watch');
            $table->date('end_date')->nullable();
            $table->bigInteger('show_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables');
    }
}
