<?php

use App\Http\Controllers\DownloadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $show = \App\Models\Show::find(1);
    $a = get_class($show->cardImages()->getRelated());
    $ci = new $a;
    dd($ci);
    return view('welcome');
});

Route::get('download', [DownloadController::class, 'index']);
